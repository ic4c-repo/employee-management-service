package com.portal.employee.employeemanagementservice.rest.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.portal.employee.employeemanagementservice.domain.Employee;
import com.portal.employee.employeemanagementservice.dto.EmployeeDTO;
import com.portal.employee.employeemanagementservice.service.EmployeeService;

import io.micrometer.core.annotation.Timed;

/**
 * @author vasudev007
 *
 */
@RestController
@RequestMapping(value = "/api/v1/")
public class EmployeeController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmployeeService employeeeService;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * GET /employees -> get list of employees
	 * 
	 * @return ResponseEntity<List<CreditCardDTO>>
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<EmployeeDTO>> getAllEmployees() {
		log.debug("Request to get list of the employees info");
		List<Employee> employees = employeeeService.findAll();
		return new ResponseEntity<>(
				employees.stream().map(employee -> convertEmployeeToEmployeeDTO(employee)).collect(Collectors.toList()), HttpStatus.OK);
	}

	/**
	 * GET /employees/:id -> get the "id" employee.
	 */
	@RequestMapping(value = "/employees/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeDTO> getEmployee(@PathVariable Long id, HttpServletResponse response) {
		log.debug("REST request to get Emplyee with Id : {}", id);
		Employee employee = employeeeService.findOneById(id);
		if (employee == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(convertEmployeeToEmployeeDTO(employee), HttpStatus.OK);
	}

	private EmployeeDTO convertEmployeeToEmployeeDTO(Employee cc) {
		EmployeeDTO ccDTO = modelMapper.map(cc, EmployeeDTO.class);
		return ccDTO;
	}

}
