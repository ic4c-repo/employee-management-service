package com.portal.employee.employeemanagementservice.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Entity class for address
 *
 */
@Entity
@Table(name = "address")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Address implements Serializable {

	private static final long serialVersionUID = -1816861647971667506L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", nullable = false)
    private Long id;
    
    @Column(name = "LINE1")
    private String line1;
    
    @Column(name = "LINE2")
    private String line2;
    
    @ManyToOne
    private City city;

    @ManyToOne
    private Country country;

}
