package com.portal.employee.employeemanagementservice.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A City.
 */
@Entity
@Table(name = "CITY")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class City implements Serializable {

	private static final long serialVersionUID = 3536376593909782903L;

	@Id
	@SequenceGenerator(name = "CITY_GENERATOR", sequenceName = "CITY_SEQ")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CITY_GENERATOR")
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "NAME")
	@Size(max = 255)
	private String name;

	@Column(name = "ZIP_CODE")
	@Size(max = 25)
	private String zipCode;

	@ManyToOne
	@NotNull
	private Country country;

	public City() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		City city = (City) o;

		if (!Objects.equals(id, city.id))
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		// @formatter:off
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", this.id)
				.append("name", this.name).append("zipCode", this.zipCode).toString();
		// @formatter:on
	}
}
