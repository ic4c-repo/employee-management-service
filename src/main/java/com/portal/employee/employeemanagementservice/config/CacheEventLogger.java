package com.portal.employee.employeemanagementservice.config;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class CacheEventLogger implements CacheEventListener<Object, Object> {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void onEvent(CacheEvent<? extends Object, ? extends Object> cacheEvent) {
		log.info("Cache logs: {}, {}, {}", cacheEvent.getKey(), cacheEvent.getOldValue(), cacheEvent.getNewValue());
	}
}
