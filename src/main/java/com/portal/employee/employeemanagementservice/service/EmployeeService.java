package com.portal.employee.employeemanagementservice.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.portal.employee.employeemanagementservice.domain.Employee;
import com.portal.employee.employeemanagementservice.repository.EmployeeRepository;

/**
 * Service class for managing Employee.
 */
@Service
@Transactional
public class EmployeeService {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmployeeRepository employeeRepository;

	/**
	 * Get employeess by page
	 * 
	 * @param pageable
	 * @return
	 */
	@Transactional(readOnly = true)
	public Page<Employee> findAllByPage(Pageable pageable) {
		log.trace("Finding all employees by page");
		return employeeRepository.findAll(pageable);
	}

	/**
	 * Get all employees
	 * 
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<Employee> findAll() {
		log.trace("Finding all employees");
		return employeeRepository.findAll();
	}

	/**
	 * Get an employee by Id
	 * 
	 * @param id
	 * @return
	 */
	@Cacheable(value = "employeeCache", 
      key = "#id")
	public Employee findOneById(Long id) {
		log.trace("Finding one employee using eagar");
		return employeeRepository.findOneById(id);
	}

}
