package com.portal.employee.employeemanagementservice.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * DTO for credit card
 * 
 * @author vasudev007
 *
 */
public class EmployeeDTO {

	private String name;

	private String employeeId;

	private LocalDateTime dob;

	//private AddressDTO addressDTO;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public LocalDateTime getDob() {
		return dob;
	}

	public void setDob(LocalDateTime dob) {
		this.dob = dob;
	}

/*	public AddressDTO getAddressDTO() {
		return addressDTO;
	}

	public void setAddressDTO(AddressDTO addressDTO) {
		this.addressDTO = addressDTO;
	}*/

	@Override
	public String toString() {
		// @formatter:off
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("name", this.name)
				.append("employeeId", this.employeeId).append("dob", this.dob)
				//.append("addressDTO", this.addressDTO)
				.toString();
		// @formatter:on
	}

}
