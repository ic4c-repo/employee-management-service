package com.portal.employee.employeemanagementservice.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.portal.employee.employeemanagementservice.domain.Employee;

/**
 * Repository class to manage credit card entity
 *
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	Page<Employee> findAll(Pageable pageable);

	List<Employee> findAll();

	Employee findOneById(@Param("id") Long id);

}
