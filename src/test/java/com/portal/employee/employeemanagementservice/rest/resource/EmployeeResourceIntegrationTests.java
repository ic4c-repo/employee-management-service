package com.portal.employee.employeemanagementservice.rest.resource;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.portal.employee.employeemanagementservice.dto.EmployeeDTO;

/**
 * Integration test class, activated in staging profile
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("staging")
public class EmployeeResourceIntegrationTests {

	private static final String hostName = "http://localhost:";

	private static final Long employeeIdInDB = 100L;

	private static final Long employeeIdNotInDB = 1023240L;

	private static final String employeeIdNotLong = "10a24";

	private static final String uri = "/api/v1/employees/";

	private String fullHostName;

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Before
	public void setUp() {
		fullHostName = new StringBuilder().append(hostName).append(port).append(uri).toString();
	}

	@Test
	public void shouldReturnEmployeeDTOListWhenGetEmployeeIdInDB() {
		assertThat(this.restTemplate.getForObject(fullHostName + employeeIdInDB, EmployeeDTO.class)).isNotNull();
	}

	@Test
	public void shouldReturnNullWhenGetEmployeeIdNotInDB() {
		EmployeeDTO em = this.restTemplate.getForObject(fullHostName + employeeIdNotInDB, EmployeeDTO.class);
		//assertThat(em).isNull();
	}

	@Test
	public void shouldReturnBadRequestWhenGetEmployeeIdNotLong() {
		EmployeeDTO em = this.restTemplate.getForObject(fullHostName + employeeIdNotLong, EmployeeDTO.class);
		assertThat(em.getEmployeeId()).isNull();
	}

}
