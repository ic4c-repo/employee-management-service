package com.portal.employee.employeemanagementservice.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockingDetails;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.portal.employee.employeemanagementservice.domain.Employee;
import com.portal.employee.employeemanagementservice.repository.EmployeeRepository;

/**
 * Test class for EmployeeService
 *
 */
@RunWith(SpringRunner.class)
public class EmployeeServiceTest {

	@TestConfiguration
	static class  EmployeeServiceTestConfig {
		
		@Bean
		public EmployeeService employeeService(){
			return new EmployeeService();			
		}
	};

	private static final Long employeeInDB = 100L;
	
	private static final Long employeeNotInDB = 1000L;
	
	@Autowired
	private EmployeeService toTest;
	
	@MockBean
	private EmployeeRepository employeeRepository;

	@Before
	public void setUp() {
		Employee em = new Employee();
		em.setId(employeeInDB);
		em.setName("testEmployee");
		
		Mockito.when(employeeRepository.findOneById(employeeInDB)).thenReturn(em);
		
		Mockito.when(employeeRepository.findOneById(employeeNotInDB)).thenReturn(null);
	}

	@Test
	public void whenFindOneById_thenReturnEmployeeWithId() {
		Employee actual = toTest.findOneById(employeeInDB);
		assertThat(actual).isNotNull();
	}
	
	@Test
	public void whenFindOneByIdNotInDB_thenReturnEmployeeWithId() {
		Employee actual = toTest.findOneById(employeeNotInDB);
		assertThat(actual).isNull();
	}

}
