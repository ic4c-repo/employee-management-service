package com.portal.employee.employeemanagementservice.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.portal.employee.employeemanagementservice.domain.Employee;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EmployeeRepository toTest;

	private static final Long employeeInDB = 100L;

	private static final Long employeeNotInDB = 100000L;

	@Test
	public void whenFindOne_thenReturnEmployee() {
		
		Employee em = new Employee();
		em.setId(employeeInDB);
		em.setName("testEmployee");
		em.setDob(null);
		entityManager.merge(em);
		entityManager.flush();
		
		// when
		Employee foundOne = toTest.findOneById(employeeInDB);
		// then
		assertThat(foundOne).isNotNull();
	}

	@Test
	public void whenNotCreated_thenReturnNull() {
		// when
		Long emId = employeeNotInDB;
		// given
		Employee foundOne = toTest.findOneById(emId);
		// then
		assertThat(foundOne).isNull();
	}

}
